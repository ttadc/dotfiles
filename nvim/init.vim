call plug#begin()
Plug 'ctrlpvim/ctrlp.vim'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'leafgarland/typescript-vim'
"Plug 'mattn/emmet-vim'
"Plug 'matveyt/vim-modest'
Plug 'noahfrederick/vim-hemisu'
Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'
"Plug 'sjl/badwolf'
Plug 'tpope/vim-fugitive'
Plug 'uarun/vim-protobuf'
Plug 'dense-analysis/ale'
call plug#end()

colo hemisu

filetype plugin indent on

se bs:2
se cino=:0
se spell spelllang=en_us
se stl=\ \%f%m%r%h%w\ ::\ %y\ [%{&ff}]\%=\ [%p%%:\ %l/%L]\ 
se tgc vb ru nu nuw:5 scl:yes nofen mouse:a bg:dark cc:80
se wig+=*.class
se wig+=classes
se wig+=dist
se wig+=.git
se wig+=.hg
se wig+=node_modules
se wig+=*.o
se wig+=*.pyc
se wig+=.svn

"let g:ale_sign_error = '✗'
let g:ale_sign_error = '→'
let g:ale_sign_warning = '→'
let g:ale_fix_on_save = 1
let g:ale_fixers = {
	\'javascript': ['prettier', 'eslint'],
	\'typescript': ['prettier', 'tslint'],
\}

" ::: Go
let g:go_template_autocreate = 0
let g:go_fmt_command="goimports"
" ::: JavaScript
"autocmd FileType javascript setlocal formatprg=prettier\ --parser\ babel
"autocmd FileType typescript setlocal formatprg=prettier\ --parser\ typescript
"let g:javascript_plugin_jsdoc = 1
" :::

" Output the current syntax group
nnoremap <f10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<cr>
