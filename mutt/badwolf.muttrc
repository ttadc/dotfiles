# Taken from the Badwolf color scheme.
# https://github.com/sjl/badwolf

# Copyright (C) 2012 Steve Losh and Contributors
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# **THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.**
 


color normal            color15         color233
color error             brightcolor15   color196
color indicator         color15         color235
color status            brightcolor39   color233
color markers           color142        color233

color hdrdefault        color245        color233
color header            brightcolor214  color233        "^(Subject): "
color signature         color243        color233

color body              color137        color233        [\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+
color body              color137        color233        ((https?|ftp)://|www)+[\-\.\;@,/%~_:?&=\#a-zA-Z0-9+]+
color bold              brightcolor15   color233
color search            brightcolor16   color221

color quoted            color243        color233
color quoted1           color211        color233
color quoted2           color222        color233
color quoted3           color214        color233
color quoted4           color154        color233

color tilde             color240        color233
color tree              color240        color233
color attachment        brightcolor211  color233

# http://www.mutt.org/doc/manual/manual-4.html#ss4.2
color index             color241        color233        ~A
color index             color241        color233        ~Q
color index             color243        color233        ~P
# --
color index             color221        color233        ~N
color index             brightcolor211  color233        ~F
color index             brightcolor39   color233        ~T
color index             color243        color233        ~Q
color index             color196        color233        ~D

# Colorize inline diffs, really helpful for LKML or other mailing lists where
# mails frequently include patches.
color body              brightcolor15   color233        '^diff .*'
color body              brightcolor15   color233        '^[Ii]{1}ndex .*'
color body              brightcolor15   color233        '^=+$'
color body              brightcolor15   color233        '^(---|\+\+\+) .*'
color body              brightcolor15   color233        '^@@ .*'
color body              color121        color233        '^\+([^\+].*)?$'
color body              color211        color233        '^-([^-].*)?$'
