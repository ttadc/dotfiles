PREFIX=${HOME}

help:
	@echo "This will clober your existing dot files.  Don't do it."
	@echo
	@echo "  $$ make install"
	@echo

install:
	install vim/vimrc ${PREFIX}/.vimrc
	install vim/gvimrc ${PREFIX}/.gvimrc
	install emacs/init.el ${PREFIX}/.emacs.d/
	install tcsh/tcshrc ${PREFIX}/.tcshrc
	install tmux/tmux.conf ${PREFIX}/.tmux.conf
	install git/gitconfig ${PREFIX}/.gitconfig
