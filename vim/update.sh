#!/bin/sh

update_module() {
        module=$1
        echo "update ${module}..."
        (cd ~/.vim/bundle/${module} && git pull)
}

modules="MatchTagAlways badwolf cql-vim ctrlp.vim emmet-vim html5.vim"
modules="${modules} ir_black seoul256.vim vim-fugitive vim-go vim-hemisu"
modules="${modules} vim-hybrid vim-ledger vim-maven-plugin vim-repeat"
modules="${modules} vim-scala vim-surround"

for module in ${modules}; do
        update_module ${module}
done

