# dotfiles

Configuration files that I use.  I use this project to help bootstrap a new
system and to backup my configuration.  It's heavily customized for me, but on
the other hand I don't see any reason to keep it private.

