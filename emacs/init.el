;;; .emacs.d/init.el --- customized Emacs options for fuchikoma.local
;;
;;; Author: David Forster
;;
;;; Commentary:
;;  This is a collection of various snippets of Emacs customations and
;;  Elisp that I've collected over the years.  I prefer a minimal
;;  Emacs configuration, staying as close to the default settings as
;;  possible, while providing a few extra features and `personal
;;  taste' enhacements.
;;
;;; Code:

;; Add a location for user-specific Elisp code.
(add-to-list 'load-path "~/.emacs.d/lisp")

;; Initialize the now built in package.el package and add the MELPA
;; archvie.  MELPA provides a comprehensive collection of Emacs
;; packages and seems to be pretty commonly referred to.
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(setq package-enable-at-startup nil)

;; Authentication information is stored in ~/.authoinfo.  For GMail,
;; it should look like:
;;
;; machine imap.gmail.com login <email> password <password>
;; machine imap.gmail.com login <email> password <password> port 993
;; machine smtp.gmail.com login <email> password <password> port 587
;;
(setq user-full-name "David Patrick Forster")
(setq user-mail-address "d@tunedtoadeadchannel.com")
(setq send-mail-function 'sendmail-sent-it)

;; When using EMACS for macOS, the path doesn't get set correctly,
;; since macOS doesn't evaulate a startup script when you login.
(when (string-equal system-type "darwin")
  (setq exec-path (append exec-path '("/usr/local/bin"))))

;; Use mail-mode for anything we're editing from Mutt.
(add-to-list 'auto-mode-alist '("/mutt" . mail-mode))

;; Reduce the amount of visual clutter on the screen at startup and
;; while in use.  Almost all of these features are geared towards
;; novice users.
(setq inhibit-splash-screen t)
(setq inhibit-startup-echo-area-message t)
(menu-bar-mode 0)
(if window-system
    (progn
      (scroll-bar-mode 0)
      (tool-bar-mode 0)))


;(column-number-mode t)

;; I often use EMACS full screen, so it's useful to see the time.
(display-time-mode t)

;; I miss the good old days of the 2400 baud as much as the next
;; hacker, but unfortunately that isn't that much.
(setq scroll-step 1)

;; On MacOS X (NextStep) the font is set through Elisp, whereas on X11
;; it's set as a resource file (usally ~/.Xresources).
(if (eq window-system 'ns)
    (progn
      (set-face-attribute 'default nil :font "Terminus (TTF)" :height 200)
      (setq ns-antialias-text nil)))

;; I'd set the font here to mirror macOS, but setting it via an X
;; resource causes it to be set sooner, before the window is mapped.
;(if (eq window-system 'x)
;    (progn
;      (set-face-attribute 'default nil :font "M+ 1m" :height 140 :weight 'light)))

;; I prefer a dark theme as it's just easier on my eyes, especially at
;; night.
(load-theme 'hemisu-dark t)
;(require 'badwolf-theme)
;(require 'peacock-theme)
;(require 'color-theme-warez)
;(require 'color-theme-molokai)


;; Go
;;
;; Run "go fmt" on a buffer before it is saved. 
(add-hook 'before-save-hook 'gofmt-before-save)


;; C
;;
(add-hook 'c-mode-hook
          (lambda ()
            "Custom C mode hook"
            (define-key c-mode-map "\C-m" 'reindent-then-newline-and-indent)
            (c-set-style "bsd")))

;; org-mode
;;
(add-hook 'org-mode-hook
	  (lambda ()
	    "Custom Org mode hook"
	    (remove-face-heights)))

;; Store backup files in another location so that they don't clutter
;; up the working directory.
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq delete-old-versions -1)
(setq version-control t)
(setq vc-make-backup-files t)
;(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))


;; Winner mode allows for quickly navigating between old and new
;; window layouts.
(winner-mode 1)

;; GNUS configuration.  Credentials are stored in "~/.authinfo".
;;
;; I don't really use GNUS but for some reason I try it out every
;; couple of years.  I like the idea, but as a mail client it's just
;; awful.
(setq gnus-select-method '(nnnil ""))
(setq gnus-secondary-select-methods
      '((nntp "news.gmane.org")
	(nnimap "imap.gmail.com"
		(nnimap-address "imap.gmail.com")
		(nnimap-server-port 993)
		(nnimap-stream ssl)
		(nnimap-authenticator login))))


;; Keybindings


(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

;; I would like to remove face heights (diffrent font sizes) within
;; EMACS, but there doesn't appear to be a good way to do it, at least
;; what I've found.
(defun remove-face-heights ()
  "Remove face height from all faces, except 'default.'  What a STUPID feature."
  (interactive)
  (mapc
   (lambda (face)
     (unless (eq face 'default)
       (set-face-attribute face nil :height 'unspecified)))
   (face-list)))

(add-hook 'after-init-hook 'remove-face-heights)


(provide 'init)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("7feeed063855b06836e0262f77f5c6d3f415159a98a9676d549bfeb6c49637c4" default)))
 '(package-selected-packages
   (quote
    (hemisu-theme exec-path-from-shell go-guru solarized-theme w3m use-package scala-mode2 sbt-mode peacock-theme markdown-mode ledger-mode helm go-mode ess color-theme-solarized badwolf-theme))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(eval-after-load "info" '(copy-face 'default 'Info-quoted))
