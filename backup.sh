#!/bin/sh

RECIPIENT="A810E0D294FFA236E4FB2D740A6C56D1C9A3A2AB"
OUTGOING=${HOME}/outgoing

#PREFIX=`date "+%Y-%m-%d "`
PREFIX=

ARCHIVE="${OUTGOING}/${PREFIX}secure.tar.xz"
tar -C ${HOME} -Jcf "${ARCHIVE}" -- secure
rm -f "${ARCHIVE}.gpg"
gpg --batch -e -r ${RECIPIENT} "${ARCHIVE}"
rm -f "${ARCHIVE}"
echo "${ARCHIVE}"

ARCHIVE="${OUTGOING}/${PREFIX}doc-n.tar.xz"
tar -C ${HOME} -Jcf "${ARCHIVE}" -- doc/n
rm -f "${ARCHIVE}.gpg"
gpg --batch -e -r ${RECIPIENT} "${ARCHIVE}"
rm -f "${ARCHIVE}"
echo "${ARCHIVE}"

